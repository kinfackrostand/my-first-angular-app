import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[]=[
    new Recipe('A test Recipe','is the best of the best','https://media.istockphoto.com/id/136503658/fr/photo/cuisine-tha%C3%AFlandaise-d%C3%A9licieux.jpg?s=612x612&w=is&k=20&c=sf4WJLu25GI9YazqIAU6QNWQevzf9LZCsGSP9T8ULMw='),
    new Recipe('A test Recipe','is the best of the best','https://media.istockphoto.com/id/136503658/fr/photo/cuisine-tha%C3%AFlandaise-d%C3%A9licieux.jpg?s=612x612&w=is&k=20&c=sf4WJLu25GI9YazqIAU6QNWQevzf9LZCsGSP9T8ULMw=')
  ];

  constructor(){

  }
  ngOnInit(): void {
    
  }


}
